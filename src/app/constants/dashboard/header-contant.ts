export const headerConstant = {
    companyName: "Company Name",
    companyLogo: "https://img.icons8.com/plasticine/2x/company.png",
    profileMenu: {
        profile: "Profile",
        settings: "Settings",
        logOut: "LogOut"
    }
}