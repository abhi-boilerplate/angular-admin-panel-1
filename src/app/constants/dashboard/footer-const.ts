export const footerConstant = {
   copyRight: 'Company name - All right reserved',
   copyRightYear: new Date().getFullYear(),
   otherText: 'Some other text here'
}