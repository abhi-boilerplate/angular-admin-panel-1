import { Component, OnInit, ViewChild } from '@angular/core';
import { SideNavComponent } from '../side-nav/side-nav.component';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  @ViewChild('sideNavComponent', { static: false })
  public sideNavComponent: SideNavComponent;




  constructor() { }

  ngOnInit(): void {
  }



  public showNav() {
    this.sideNavComponent.navEvent();
  }

}
