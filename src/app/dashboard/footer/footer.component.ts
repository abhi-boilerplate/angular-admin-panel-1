import { Component, OnInit } from '@angular/core';
import { footerConstant } from 'src/app/constants/dashboard/footer-const';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public staticText = footerConstant;

  constructor() { }

  ngOnInit(): void {
  }

}
