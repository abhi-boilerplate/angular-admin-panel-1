import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { DefaultComponent } from './default/default.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ErrorPageComponent } from './error-page/error-page.component';



@NgModule({
  declarations: [DashboardComponent, HeaderComponent, FooterComponent, SideNavComponent, DefaultComponent, ErrorPageComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  exports: [
    DashboardComponent, DefaultComponent
  ]
})
export class DashboardModule { }
