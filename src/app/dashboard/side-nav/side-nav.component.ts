import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {


  @ViewChild('sideNavDiv', { static: false })
  sideNavDiv: ElementRef;

  @ViewChild('sideNavSmall', { static: false })
  sideNavSmall: ElementRef;

  @ViewChild('sideNavImg1', { static: false })
  sideNavImg1: ElementRef;

  @ViewChild('sideNavImg2', { static: false })
  sideNavImg2: ElementRef;


  @ViewChild('textNav1', { static: false })
  textNav1: ElementRef;


  @ViewChild('textNav2', { static: false })
  textNav2: ElementRef;


  constructor(private renderer: Renderer2) { }


  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.navEvent();

  }

  public navEvent(): void {
    const navView = this.sideNavDiv.nativeElement.classList.contains('nav-style');
    this.styleReset(navView);
    if (window.innerWidth <= 800 && window.innerWidth >= 600) {
      this.styleLess800(navView);
    }
    else if (window.innerWidth <= 600) {
      this.styleLess600(navView);
    }
  }

  private styleReset(navView: boolean): void {
    this.renderer[navView ? 'removeClass' : 'addClass'](this.sideNavDiv.nativeElement, 'nav-style');
    this.renderer.setStyle(this.textNav1.nativeElement, 'display', "block");
    this.renderer.setStyle(this.textNav2.nativeElement, 'display', "block");
    this.renderer.setStyle(this.sideNavImg1.nativeElement, 'display', "block");
    this.renderer.setStyle(this.sideNavImg2.nativeElement, 'display', "block");
    this.renderer.setStyle(this.sideNavSmall.nativeElement, 'display', 'none');
  }

  private styleLess800(navView: boolean): void {
    this.renderer[navView ? 'addClass' : 'removeClass'](this.sideNavDiv.nativeElement, 'nav-style-small');
    if (navView) {
      this.renderer.setStyle(this.sideNavSmall.nativeElement, 'display', 'none');
      this.renderer.setStyle(this.sideNavImg1.nativeElement, 'display', "block");
      this.renderer.setStyle(this.sideNavImg2.nativeElement, 'display', "block");
    }
    else {
      this.renderer.setStyle(this.sideNavSmall.nativeElement, 'display', 'block');
      this.renderer.setStyle(this.sideNavImg1.nativeElement, 'display', "none");
      this.renderer.setStyle(this.sideNavImg2.nativeElement, 'display', "none");
    }
  }

  private styleLess600(navView: boolean): void {
    this.renderer[navView ? 'addClass' : 'removeClass'](this.sideNavSmall.nativeElement, 'nav-style-mobile');
    this.renderer.setStyle(this.sideNavSmall.nativeElement, 'display', 'none');
    this.renderer.setStyle(this.sideNavImg1.nativeElement, 'display', "block");
    this.renderer.setStyle(this.sideNavImg2.nativeElement, 'display', "block");
    if (navView) {
      this.renderer.setStyle(this.textNav1.nativeElement, 'display', "block");
      this.renderer.setStyle(this.textNav2.nativeElement, 'display', "block");
    }
    else {
      this.renderer.setStyle(this.textNav1.nativeElement, 'display', "none");
      this.renderer.setStyle(this.textNav2.nativeElement, 'display', "none");
    }
  }
}
