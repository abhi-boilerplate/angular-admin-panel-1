import { Component, ElementRef, EventEmitter, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { headerConstant } from 'src/app/constants/dashboard/header-contant';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('profileMenu', {static: false})
  public profileMenu: ElementRef;

  @Output()
  public navEventHander= new EventEmitter();

  public staticText= headerConstant;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }

  public profileClick() {
    const active = this.profileMenu.nativeElement.classList.contains('is-active');
    this.renderer[active ? 'removeClass' : 'addClass'](this.profileMenu.nativeElement, 'is-active')
  }

  public navEvent(){
    this.navEventHander.emit();
  }

}
